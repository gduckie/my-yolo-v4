import subprocess
import argparse
import os
from PIL import Image
from random import randint


# Ex:
# 1. python3 add_background.py -s source -d destination
# 2. python3 add_background.py --source=source --destination=destination
#Read arguments

ap = argparse.ArgumentParser()
# source
ap.add_argument("-s", "--source", required=True,
    help="path to source file")
# destination
ap.add_argument("-d", "--destination", required=True,
    help="path to dataset folder")

args = vars(ap.parse_args())


source = args["source"]
destination = args["destination"]
processed = 0
bg = [{"source": './background_img/black_bg.png', "name": 'black_bg'}, {"source": './background_img/colorful_bg.png', "name": 'colorful_bg'}, {"source": './background_img/document_bg.png', "name": 'document_bg'}, {"source": './background_img/white_bg.png', "name": 'white_bg'}]


type_enum = {
    "error": 0,
    "success": 1
}
def log(message_type, message):
    if message_type == type_enum["error"]:
        print("{" + "type: 'Error', message: '{}'".format(message) + "}")
    else:
        print("{" + "{type: 'Success', message: '{}'}".format(message) + "}")

def log_info(processed, total):
    print("json{" + "\"processed\": \"{}\", \"total\": \"{}\"".format(processed, total) +"}")


def get_total_image(source_folder):
    tail = ['png', 'jpg', 'gif', 'jpeg']
    total = 0
    for count, fil in enumerate(os.listdir(source_folder)):
        for ext in tail:
            if ext in fil:
                total += 1
                continue
    return total

def overlay(bg_source, bg_name, dest_folder, source_folder):
    global processed
    tail = ['png', 'jpg', 'gif', 'jpeg']
    # load image and background
    total_img = get_total_image(source_folder)
    total = str(total_img * len(bg))

    img_read = 0
    for count, filename in enumerate(os.listdir(source_folder)):
        background = Image.open(bg_source).convert('RGBA')
        bg_w, bg_h = background.size
        if filename.split(".")[-1] in tail:

            img = Image.open(source_folder + '/' +  filename)
            print("\n==========================================================\n")
            # print('./' + source_folder + '/' +  filename)
            img_w, img_h = img.size
            if not os.path.isfile('./' + source_folder + '/' +  filename):
                break
            # Make sure img_w < bg_w
            if img_w > bg_w or img_h > bg_h:
                # print('resize ' + source_folder + '/' +  filename)
                newsize = (500, 500)
                img = img.resize(newsize)
                img.save('./' + source_folder + '/' +  filename, 'png')
                img_w = 500
                img_h = 500
                # print('Cannot write this:')
                # print('./' + source_folder + '/' +  filename)
                # continue

            random_offset_x = randint(0, bg_w - img_w) # bg_w - img_w: To make sure image does not be overflow on background
            random_offset_y = randint(0, bg_h - img_h) # bg_h - img_h: To make sure image does not be overflow on background


            offset = (random_offset_x, random_offset_y)
            # Overlay image on background with random offset
            background.paste(img, offset)
            # background.save(dest_folder + '/' + bg_name + os.path.splitext(filename)[0] + '.png')
            print("Write: " + "{}/{}_{}_{}.{}".format(dest_folder, bg_name, os.path.splitext(filename)[0], count, "png"))
            background.save("{}/{}_{}_{}.{}".format(dest_folder, bg_name, os.path.splitext(filename)[0], count, "png"))

            # calculate xmin, ymin, xmax, ymax
            xmin = random_offset_x
            ymin = random_offset_y
            xmax = random_offset_x + img_w
            ymax = random_offset_y + img_h

            # calculate yolo format
            x_center = float((xmin + xmax)) / 2 / bg_w
            y_center = float((ymin + ymax)) / 2 / bg_h
            w = float((xmax - xmin)) / bg_w
            h = float((ymax - ymin)) / bg_h
            # Create train file (txt) and write it with above xmin, ymin, xmax, ymax

            with open("{}/{}_{}_{}.{}".format(dest_folder, bg_name, os.path.splitext(filename)[0], count, 'txt'), 'w') as f:
                print("Write: " + "{}/{}_{}_{}.{}".format(dest_folder, bg_name, os.path.splitext(filename)[0], count, 'txt'))
                f.write("%d %.6f %.6f %.6f %.6f\n" % (0, x_center, y_center, w, h))
            processed += 1
            log_info(str(processed), total)
            # with open(dest_folder + '/' + bg_name + os.path.splitext(filename)[0] + '.txt', 'w') as f:
            #     f.write("%d %.6f %.6f %.6f %.6f\n" % (0, x_center, y_center, w, h))
            # file = open(dest_folder + bg_name + os.path.splitext(filename)[0] + '.txt', 'w+')
            background.close()
            img.close()


def main():
    if source is not None and not os.path.isdir(source):
        log(type_enum["error"], "source folder is not existed")
        return

    if destination is not None and not os.path.isdir(destination):
        os.mkdir(destination)

    for background in bg:
        overlay(background["source"], background["name"], destination, source)
        print(background["source"])

if __name__ == '__main__':

    # Calling main() function
    main()
