# B-detector project using Yolov4 Darknet

## Prerequisites
- Docker installed
- If you have NVIDIA graphic card, following this installation to install NVIDIA container runtime to run this project by GPU ([Installation Guide](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html))

## Installation
CPU:
```
docker build . -t darknet-cpu-v1-docker -f Dockerfile-cpu
```
GPU
```
docker build . -t darknet-gpu-v1-docker -f Dockerfile-gpu
```
## Command
Detect single image:
- GPU
```sh
$ docker run --runtime=nvidia --rm -v $PWD:/workspace -w /workspace darknet-gpu-v1-docker \
        detector test data/obj.data cfg/yolov4-obj.cfg yolov4-obj_last.weights -i 0 -thresh 0.5 <image-path> -ext_output
```
- CPU
```sh
$ docker run --rm -v $PWD:/workspace -w /workspace darknet-cpu-v1-docker \
        detector test data/obj.data cfg/yolov4-obj.cfg yolov4-obj_last.weights -i 0 -thresh 0.5 <image-path> -ext_output
```
Training:
Note: If you have new data, run from step 1. If you want to test, I already put some data in `obj` and `test` folder for testing, just run step `4`.
1. Copy data:
- Copy all your train data to `data/obj` folder
- Copy all your test data to `data/test` folder
3. In root folder, generate location file:
- Run `python3 generate_test.py` to generate test file direction
- Run `python3 generate_train.py` to generate train file direction
4. Run training:
- GPU
```sh
$ docker run --runtime=nvidia --rm -v $PWD:/workspace -w /workspace darknet-<cpu | gpu>-v1-docker \
        darknet detector train data/obj.data cfg/yolov4-obj.cfg yolov4.conv.137 -dont_show -map
```
- CPU
```sh
$ docker run --rm -v $PWD:/workspace -w /workspace darknet-<cpu | gpu>-v1-docker \
        darknet detector train data/obj.data cfg/yolov4-obj.cfg yolov4.conv.137 -dont_show -map
```

## New script:
### 1. Add background to current dataset:
Purpose: Create more data
- Automatically add background to each image in dataset
- Background: black, colorful, document, white

```sh
usage: add_background.py [-h] -s SOURCE -d DESTINATION

optional arguments:
  -h, --help            show this help message and exit
  -s SOURCE, --source SOURCE
                        path to source file
  -d DESTINATION, --destination DESTINATION
                        path to dataset folder
```
Example:
```sh
docker run --rm -v $PWD:/workspace -w /workspace darknet-<cpu | gpu>-v1-docker \
        python3 add_background.py -s source -d destination
```
### 2. Copy file:
- Automatically copy data from `source` dataset to `destination`
- Automatically split data into 80% in train and 20% test

Note: In training script, it also includes copy dataset to `data` folder. This script is just used if you want to copy by yourself.
```sh
usage: copy_files_train.py [-h] -d DESTINATION -s SOURCE

optional arguments:
  -h, --help            show this help message and exit
  -d DESTINATION, --destination DESTINATION
                        path to destination directory of images
  -s SOURCE, --source SOURCE
                        path to source directory of images
```
Example:
```sh
docker run --rm -v $PWD:/workspace -w /workspace darknet-<cpu | gpu>-v1-docker \
       python3 copy_files_train.py -s ./train/obj -d ./train/train_0909
```
### 3. Training:
- Automatically copy data from `dataset` to `data` folder
- Allow users to set max iterations
- Allow users to set weights saving folder
- Automatically generate train + test file
- Run training with config above

```sh
optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG (required)
                        path to config file
  -d DATASET, --dataset DATASET (required)
                        path to dataset folder
  -m MAX_ITERATIONS, --max_iterations MAX_ITERATIONS (optional)
                        set max-iterations you want to train. If not set, the
                        default value is 6000
  -w WEIGHTS_FOLDER, --weights_folder WEIGHTS_FOLDER (required)
                        path to weights folder you want to save
  -wl WEIGHTS_LAST, --weights_last WEIGHTS_LAST (optional)
                        path to last weights file that you want continuing
                        training from last weights
```


Example:
```sh
docker run --rm -v $PWD:/workspace -w /workspace darknet-<cpu | gpu>-v1-docker \
        python3 train.py --config=cfg/yolov4-obj.cfg --dataset=dataset --max_iterations=6000 --weights_folder=data --weights_last=yolov4-obj_last.weights
```
### 4. Test
- Automatically testing with dataset

format:

```sh
{
    processed: number,
    total: number,
    duration: number,
    accuracy: number,
    avgDuration: number,
    avgAccuracy: number
}
```

```sh
usage: test.py [-h] -s SOURCE [-c CONFIG] [-w WEIGHT]

optional arguments:
  -h, --help            show this help message and exit
  -s SOURCE, --source SOURCE
                        path to data test folder
  -c CONFIG, --config CONFIG
                        path to config file (optional). If not set, default is 'cfg/yolov4-obj.cfg'
  -w WEIGHT, --weight WEIGHT
                        path to weight file (optional). If not set, default is 'yolov4-obj_last.weights'

```

Example:
```sh
docker run -it --rm -v $PWD:/workspace -w /workspace darknet-cpu-v1-docker python3 test.py -s test
```
